package com.example.demo.controller;

import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.models.Kanban;
import com.example.demo.models.Task;
import com.example.demo.pojo.KanbanDTO;
import com.example.demo.pojo.TaskDTO;
import com.example.demo.service.KanbanService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/todo-backend/kanbans")
@CrossOrigin(origins = "http://localhost:4200")
public class KanbanController {

	@Autowired
	private KanbanService kanbanService;

	@GetMapping("/")
	@ApiOperation(value = "View a list of all Kanban boards", response = Kanban.class, responseContainer = "List")
	public ResponseEntity<?> getAllKanbans() {
		try {
			return new ResponseEntity<>(kanbanService.getAllKanbanBoards(), HttpStatus.OK);
		} catch (Exception e) {
			return errorResponse();
		}
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Find a Kanban board info by its id", response = Kanban.class)
	public ResponseEntity<?> getKanban(@PathVariable UUID id) {
		try {
			Optional<Kanban> optKanban = kanbanService.getKanbanById(id);
			if (optKanban.isPresent()) {
				return new ResponseEntity<>(optKanban.get(), HttpStatus.OK);
			} else {
				return noKanbanFoundResponse(id);
			}
		} catch (Exception e) {
			return errorResponse();
		}
	}

	@GetMapping("")
	@ApiOperation(value = "Find a Kanban board info by its title", response = Kanban.class)
	public ResponseEntity<?> getKanbanByTitle(@RequestParam String title) {
		try {
			Optional<Kanban> optKanban = kanbanService.getKanbanByTitle(title);
			if (optKanban.isPresent()) {
				return new ResponseEntity<>(optKanban.get(), HttpStatus.OK);
			} else {
				return new ResponseEntity<>("No kanban found with a title: " + title, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return errorResponse();
		}
	}

	@PostMapping("/")
	@ApiOperation(value = "Save new Kanban board", response = Kanban.class)
	public ResponseEntity<?> createKanban(@RequestBody KanbanDTO kanbanDTO) {
		try {
			return new ResponseEntity<>(kanbanService.saveNewKanban(kanbanDTO), HttpStatus.CREATED);
		} catch (Exception e) {
			return errorResponse();
		}
	}

	@PutMapping("/{id}")
	@ApiOperation(value = "Update a Kanban board with specific id", response = Kanban.class)
	public ResponseEntity<?> updateKanban(@PathVariable UUID id, @RequestBody KanbanDTO kanbanDTO) {
		try {
			Optional<Kanban> optKanban = kanbanService.getKanbanById(id);
			if (optKanban.isPresent()) {
				return new ResponseEntity<>(kanbanService.updateKanban(optKanban.get(), kanbanDTO), HttpStatus.OK);
			} else {
				return noKanbanFoundResponse(id);
			}
		} catch (Exception e) {
			return errorResponse();
		}
	}

	@DeleteMapping("/{id}")
	@ApiOperation(value = "Delete Kanban board with specific id", response = String.class)
	public ResponseEntity<?> deleteKanban(@PathVariable UUID id) {
		try {
			Optional<Kanban> optKanban = kanbanService.getKanbanById(id);
			if (optKanban.isPresent()) {
				kanbanService.deleteKanban(optKanban.get());
				return new ResponseEntity<>(String.format("Kanban with id: %d was deleted", id), HttpStatus.OK);
			} else {
				return noKanbanFoundResponse(id);
			}
		} catch (Exception e) {
			return errorResponse();
		}
	}

	@GetMapping("/{kanbanId}/tasks/")
	@ApiOperation(value = "View a list of all tasks for a Kanban with provided id", response = Task.class, responseContainer = "List")
	public ResponseEntity<?> getAllTasksInKanban(@PathVariable UUID kanbanId) {
		try {
			Optional<Kanban> optKanban = kanbanService.getKanbanById(kanbanId);
			if (optKanban.isPresent()) {
				return new ResponseEntity<>(optKanban.get().getTasks(), HttpStatus.OK);
			} else {
				return noKanbanFoundResponse(kanbanId);
			}
		} catch (Exception e) {
			return errorResponse();
		}
	}

	@PostMapping("/{kanbanId}/tasks/")
	@ApiOperation(value = "Save new Task and assign it to Kanban board", response = Kanban.class)
	public ResponseEntity<?> createTaskAssignedToKanban(@PathVariable UUID kanbanId, @RequestBody TaskDTO taskDTO) {
		try {
			return new ResponseEntity<>(kanbanService.addNewTaskToKanban(kanbanId, taskDTO), HttpStatus.CREATED);
		} catch (Exception e) {
			return errorResponse();
		}
	}

	private ResponseEntity<String> errorResponse() {
		return new ResponseEntity<>("Something went wrong :(", HttpStatus.INTERNAL_SERVER_ERROR);
	}

	private ResponseEntity<String> noKanbanFoundResponse(UUID id) {
		return new ResponseEntity<>("No kanban found with id: " + id, HttpStatus.NOT_FOUND);
	}
}