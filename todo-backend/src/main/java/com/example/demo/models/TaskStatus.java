package com.example.demo.models;

import io.swagger.annotations.ApiModel;

@ApiModel
public enum TaskStatus {
	TODO, INPROGRESS, DONE
}