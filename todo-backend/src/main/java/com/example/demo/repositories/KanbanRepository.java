package com.example.demo.repositories;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.models.Kanban;

@Repository
public interface KanbanRepository extends JpaRepository<Kanban, UUID> {

    Optional<Kanban> findByTitle(String title);
}