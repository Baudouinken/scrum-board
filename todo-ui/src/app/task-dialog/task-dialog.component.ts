import { Component, Inject, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Task } from 'src/models/task';
import { KanbanService } from '../../service/kanban.service';
import { TaskService } from '../../service/task.service';

@Component({
  selector: 'app-task-dialog',
  templateUrl: './task-dialog.component.html',
  styleUrls: ['./task-dialog.component.css']
})
export class TaskDialogComponent implements OnInit {


  @Input()
  kanbanId: String = "";
  @Input()
  task: Task = new Task;

  form: FormGroup;

  @Input()
  isUpdate: boolean = false;

  @Input()
  title: string = "";

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<TaskDialogComponent>,
    private kanbanService: KanbanService,
    private taskService: TaskService) {

    this.form = fb.group({
      title: [this.task.title, Validators.required],
      description: [this.task.description, Validators.required],
      color: [this.task.color,Validators.required]
  });
  }

  ngOnInit() {
    if(this.isUpdate) {
      this.form.get('title')?.setValue(this.task.title);
      this.form.get('color')?.setValue(this.task.color);
      this.form.get('description')?.setValue(this.task.description);
    }
  }

  save() {
    this.mapFormToTaskModel();
    if (!this.task.id) {
      this.kanbanService.saveNewTaskInKanban(this.kanbanId, this.task).subscribe();
    } else {
      this.taskService.updateTask(this.task).subscribe();
    }
    this.dialogRef.close();
    window.location.reload();
  }

  close() {
      this.dialogRef.close();
  }

  private mapFormToTaskModel(): void {
    this.task.title = this.form.get('title')?.value;
    this.task.description = this.form.get('description')?.value;
    this.task.color = this.form.get('color')?.value;
    this.task.status = 'TODO';
  }

}
