import { Task } from "./task";

export class Kanban {

    id!: String;
    title!: String;
    tasks!: Task[];
}
